package com.alvinrusli.mandiritasknews.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.activity.core.CoreActivity
import com.alvinrusli.mandiritasknews.fragment.CategoryListFragment
import kotlinx.android.synthetic.main.activity_source_list.*

class CategoryListActivity : CoreActivity() {

    override val viewRes = R.layout.activity_category_list

    private val fragment by lazy { CategoryListFragment.newInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) initFragment()
    }

    /** Initialize the fragment */
    private fun initFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(layout_fragment.id, fragment)
        fragmentTransaction.commit()
    }

    companion object {

        /**
         * Launch this activity.
         * @param context the context
         */
        fun launchIntent(context: Context) {
            val intent = Intent(context, CategoryListActivity::class.java)
            context.startActivity(intent)
        }
    }
}
