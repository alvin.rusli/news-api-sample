package com.alvinrusli.mandiritasknews.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.activity.core.CoreActivity
import com.alvinrusli.mandiritasknews.fragment.NewsListFragment
import kotlinx.android.synthetic.main.activity_news_list.*

class NewsListActivity : CoreActivity() {

    override val viewRes = R.layout.activity_news_list

    private val source by lazy { intent.getStringExtra("source") ?: "" }
    private val fragment by lazy { NewsListFragment.newInstance(source) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) initFragment()
    }

    /** Initialize the fragment */
    private fun initFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(layout_fragment.id, fragment)
        fragmentTransaction.commit()
    }

    companion object {

        /**
         * Launch this activity.
         * @param context the context
         */
        fun launchIntent(context: Context, source: String?) {
            val intent = Intent(context, NewsListActivity::class.java)
            intent.putExtra("source", source)
            context.startActivity(intent)
        }
    }
}
