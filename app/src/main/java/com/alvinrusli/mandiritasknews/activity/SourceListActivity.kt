package com.alvinrusli.mandiritasknews.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.activity.core.CoreActivity
import com.alvinrusli.mandiritasknews.fragment.SourceListFragment
import kotlinx.android.synthetic.main.activity_source_list.*

class SourceListActivity : CoreActivity() {

    override val viewRes = R.layout.activity_source_list

    private val category by lazy { intent.getStringExtra("category") ?: "" }
    private val fragment by lazy { SourceListFragment.newInstance(category) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) initFragment()
    }

    /** Initialize the fragment */
    private fun initFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(layout_fragment.id, fragment)
        fragmentTransaction.commit()
    }

    companion object {

        /**
         * Launch this activity.
         * @param context the context
         */
        fun launchIntent(context: Context, category: String) {
            val intent = Intent(context, SourceListActivity::class.java)
            intent.putExtra("category", category)
            context.startActivity(intent)
        }
    }
}
