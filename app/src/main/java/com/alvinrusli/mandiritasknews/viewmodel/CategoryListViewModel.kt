package com.alvinrusli.mandiritasknews.viewmodel

import com.alvinrusli.mandiritasknews.model.source.Source
import com.alvinrusli.mandiritasknews.model.source.SourceListResponse

class CategoryListViewModel : SourceListViewModel() {

    override fun handleSources(response: SourceListResponse): List<Source> {
        val newDataList = ArrayList(dataList.value!!)
        response.sources?.distinctBy { it.category }?.let { newDataList.addAll(it) }
        return newDataList
    }
}
