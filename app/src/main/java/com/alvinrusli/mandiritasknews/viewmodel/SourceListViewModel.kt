package com.alvinrusli.mandiritasknews.viewmodel

import androidx.lifecycle.*
import com.alvinrusli.mandiritasknews.api.APIService
import com.alvinrusli.mandiritasknews.model.core.AppError
import com.alvinrusli.mandiritasknews.model.core.Resource
import com.alvinrusli.mandiritasknews.model.source.Source
import com.alvinrusli.mandiritasknews.model.source.SourceListResponse
import com.alvinrusli.mandiritasknews.util.Config
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class SourceListViewModel : ViewModel(), LifecycleObserver {

    val resource = MutableLiveData<Resource<Any>>()
    val dataList = MutableLiveData<List<Source>>()

    private var call: Call<SourceListResponse>? = null

    init {
        dataList.value = ArrayList()
    }

    fun onRefresh() {
        dataList.value = ArrayList()
        resource.value = Resource.success()
    }

    fun fetchData(category: String? = null) {
        resource.value = Resource.loading()

        cancel()
        call = APIService().retrofitInterface.getCategories(
            apiKey = Config.API_KEY,
            category = category)
        call?.enqueue(object : Callback<SourceListResponse> {
            override fun onResponse(call: Call<SourceListResponse>, response: Response<SourceListResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    val body = response.body()!!
                    dataList.postValue(handleSources(body))
                    resource.postValue(Resource.success())
                } else {
                    resource.postValue(Resource.error(AppError(response.code(), "Response error")))
                }
            }

            override fun onFailure(call: Call<SourceListResponse>, t: Throwable) {
                resource.postValue(Resource.error(AppError(AppError.CLIENT_UNKNOWN, t.message)))
            }
        })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun cancel() {
        call?.cancel()
    }

    open fun handleSources(response: SourceListResponse): List<Source> {
        val newDataList = ArrayList(dataList.value!!)
        response.sources?.let { newDataList.addAll(it) }
        return newDataList
    }
}
