package com.alvinrusli.mandiritasknews.viewmodel

import androidx.lifecycle.*
import com.alvinrusli.mandiritasknews.api.APIService
import com.alvinrusli.mandiritasknews.model.core.AppError
import com.alvinrusli.mandiritasknews.model.core.Resource
import com.alvinrusli.mandiritasknews.model.news.News
import com.alvinrusli.mandiritasknews.model.news.NewsListResponse
import com.alvinrusli.mandiritasknews.util.Config
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsListViewModel : ViewModel(), LifecycleObserver {

    val resource = MutableLiveData<Resource<Any>>()
    val dataList = MutableLiveData<List<News>>()

    private var call: Call<NewsListResponse>? = null

    private val pageSize = 20
    private var page = 1
    var isLoadFinished = false
        private set

    init {
        dataList.value = ArrayList()
    }

    fun onRefresh() {
        page = 1
        isLoadFinished = false
        dataList.value = ArrayList()
        resource.value = Resource.success()
    }

    fun fetchData(source: String?, query: String? = null) {
        resource.value = Resource.loading()

        call = APIService().retrofitInterface.getNews(
            apiKey = Config.API_KEY,
            pageSize = pageSize,
            page = page,
            sources = source,
            query = query)
        call?.enqueue(object : Callback<NewsListResponse> {
            override fun onResponse(call: Call<NewsListResponse>, response: Response<NewsListResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    val body = response.body()!!

                    // Set your condition to handle the end of infinite scroll behaviour
                    page++
                    val articles = body.articles ?: ArrayList()
                    if (articles.size < pageSize) isLoadFinished = true

                    val newDataList = ArrayList(dataList.value!!)
                    newDataList.addAll(articles)
                    dataList.postValue(newDataList)
                    resource.postValue(Resource.success())
                } else {
                    resource.postValue(Resource.error(AppError(response.code(), "Response error")))
                }
            }

            override fun onFailure(call: Call<NewsListResponse>, t: Throwable) {
                resource.postValue(Resource.error(AppError(AppError.CLIENT_UNKNOWN, t.message)))
            }
        })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun cancel() {
        call?.cancel()
    }
}
