package com.alvinrusli.mandiritasknews.model.news

import com.google.gson.annotations.SerializedName

class NewsListResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("totalResults")
    var totalResult: Int? = null

    @SerializedName("articles")
    var articles: List<News>? = null
}
