package com.alvinrusli.mandiritasknews.model.source

import com.google.gson.annotations.SerializedName

class SourceListResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("sources")
    var sources: List<Source>? = null
}
