package com.alvinrusli.mandiritasknews.model.source

import com.google.gson.annotations.SerializedName

data class Source(@SerializedName("id") var id: String = "") {

    @SerializedName("category")
    var category: String? = null

    @SerializedName("country")
    var country: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("language")
    var language: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("url")
    var url: String? = null
}
