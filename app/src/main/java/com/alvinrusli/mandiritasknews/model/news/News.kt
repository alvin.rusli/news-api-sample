package com.alvinrusli.mandiritasknews.model.news

import com.alvinrusli.mandiritasknews.model.source.Source
import com.google.gson.annotations.SerializedName

class News {

    @SerializedName("title")
    var title: String? = null

    @SerializedName("author")
    var author: String? = null

    @SerializedName("content")
    var content: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("publishedAt")
    var publishedAt: String? = null

    @SerializedName("source")
    var source: Source? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("urlToImage")
    var urlToImage: String? = null
}
