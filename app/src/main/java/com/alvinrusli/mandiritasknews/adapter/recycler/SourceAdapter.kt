package com.alvinrusli.mandiritasknews.adapter.recycler

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alvinrusli.extension.inflate
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.adapter.recycler.core.DataListRecyclerViewAdapter
import com.alvinrusli.mandiritasknews.model.source.Source
import kotlinx.android.synthetic.main.adapter_recycler_source.view.*

class SourceAdapter : DataListRecyclerViewAdapter<Source, SourceAdapter.ViewHolder>() {

    var onSourceClick: OnSourceClick? = null

    override fun onCreateDataViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.adapter_recycler_source))
    }

    override fun onBindDataViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val data = getDataList()[adapterPosition]
                onSourceClick?.invoke(data, adapterPosition)
            }
        }

        fun bindView() {
            val data = getDataList()[adapterPosition]
            itemView.txt_name.text = data.name
            itemView.txt_description.text = data.description
        }
    }
}

typealias OnSourceClick = ((data: Source, position: Int) -> Unit)
