package com.alvinrusli.mandiritasknews.adapter.recycler

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alvinrusli.extension.inflate
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.adapter.recycler.core.DataListRecyclerViewAdapter
import com.alvinrusli.mandiritasknews.model.news.News
import kotlinx.android.synthetic.main.adapter_recycler_news.view.*

class NewsAdapter : DataListRecyclerViewAdapter<News, NewsAdapter.ViewHolder>() {

    var onNewsClick: OnNewsClick? = null

    override fun onCreateDataViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.adapter_recycler_news))
    }

    override fun onBindDataViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val data = getDataList()[adapterPosition]
                onNewsClick?.invoke(data, adapterPosition)
            }
        }

        fun bindView() {
            val data = getDataList()[adapterPosition]
            itemView.txt_title.text = data.title
            itemView.txt_description.text = data.description
            itemView.txt_author.text = data.author
            itemView.txt_publish_date.text = data.publishedAt
        }
    }
}

typealias OnNewsClick = ((data: News, position: Int) -> Unit)
