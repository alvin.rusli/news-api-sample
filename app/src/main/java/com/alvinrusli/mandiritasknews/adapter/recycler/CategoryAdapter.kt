package com.alvinrusli.mandiritasknews.adapter.recycler

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alvinrusli.extension.inflate
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.adapter.recycler.core.DataListRecyclerViewAdapter
import com.alvinrusli.mandiritasknews.model.source.Source
import kotlinx.android.synthetic.main.adapter_recycler_category.view.*

class CategoryAdapter : DataListRecyclerViewAdapter<Source, CategoryAdapter.ViewHolder>() {

    var onCategoryClick: OnCategoryClick? = null

    override fun onCreateDataViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.adapter_recycler_category))
    }

    override fun onBindDataViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val data = getDataList()[adapterPosition]
                onCategoryClick?.invoke(data, adapterPosition)
            }
        }

        fun bindView() {
            val data = getDataList()[adapterPosition]
            itemView.txt_category.text = data.category
        }
    }
}

typealias OnCategoryClick = ((data: Source, position: Int) -> Unit)
