package com.alvinrusli.mandiritasknews.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.adapter.recycler.NewsAdapter
import com.alvinrusli.mandiritasknews.adapter.recycler.core.DataListRecyclerViewAdapter
import com.alvinrusli.mandiritasknews.fragment.core.DataListFragment
import com.alvinrusli.mandiritasknews.viewmodel.NewsListViewModel
import kotlinx.android.synthetic.main.fragment_news_list.*

class NewsListFragment private constructor() : DataListFragment() {

    override val viewRes: Int? = R.layout.fragment_news_list

    private val source: String by lazy { arguments?.getString("source") ?: "" }

    private val adapter = NewsAdapter()

    private val viewModel by lazy { ViewModelProviders.of(this).get(NewsListViewModel::class.java) }

    private var searchQuery: String? = null

    override fun initSwipeRefreshLayout(): SwipeRefreshLayout? {
        return swipe_refresh_category
    }

    override fun initRecyclerView(): RecyclerView {
        return recycler_category
    }

    @Suppress("UNCHECKED_CAST")
    override fun initRecyclerAdapter(): DataListRecyclerViewAdapter<Any, RecyclerView.ViewHolder> {
        adapter.emptyText = resources.getString(R.string.info_no_data)
        adapter.onNewsClick = { data, _ ->
            val uri = Uri.parse(data.url ?: "")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
        return adapter as DataListRecyclerViewAdapter<Any, RecyclerView.ViewHolder>
    }

    override fun initRecyclerLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        // Initial behaviour
        enableSwipeToRefresh()
        enableInfiniteScrolling()

        // View bindings
        btn_search.setOnClickListener {
            val query = txt_search.text.toString().trim()
            searchQuery = if (query.isBlank()) null else query
            onSwipeToRefresh()
        }
    }

    private fun initViewModel() {
        viewModel.resource.observe(viewLifecycleOwner, Observer {
            updateResource(it)
            if (viewModel.isLoadFinished) disableInfiniteScrolling()
        })
        viewModel.dataList.observe(viewLifecycleOwner, Observer {
            updateDataList(it)
        })
        lifecycle.addObserver(viewModel)
    }

    override fun fetchData() {
        viewModel.fetchData(source, searchQuery)
    }

    override fun onSwipeToRefresh() {
        viewModel.onRefresh()
        enableInfiniteScrolling()
        fetchData()
    }

    companion object {

        /** Obtain this fragment */
        fun newInstance(source: String): NewsListFragment {
            val fragment = NewsListFragment()
            val args = Bundle()
            args.putString("source", source)
            fragment.arguments = args
            return fragment
        }
    }
}
