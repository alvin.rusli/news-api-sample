package com.alvinrusli.mandiritasknews.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.activity.NewsListActivity
import com.alvinrusli.mandiritasknews.adapter.recycler.SourceAdapter
import com.alvinrusli.mandiritasknews.adapter.recycler.core.DataListRecyclerViewAdapter
import com.alvinrusli.mandiritasknews.fragment.core.DataListFragment
import com.alvinrusli.mandiritasknews.viewmodel.SourceListViewModel
import kotlinx.android.synthetic.main.fragment_source_list.*

class SourceListFragment private constructor() : DataListFragment() {

    override val viewRes: Int? = R.layout.fragment_source_list

    private val category: String by lazy { arguments?.getString("category") ?: "" }

    private val adapter = SourceAdapter()

    private val viewModel by lazy { ViewModelProviders.of(this).get(SourceListViewModel::class.java) }

    override fun initSwipeRefreshLayout(): SwipeRefreshLayout? {
        return swipe_refresh_category
    }

    override fun initRecyclerView(): RecyclerView {
        return recycler_category
    }

    @Suppress("UNCHECKED_CAST")
    override fun initRecyclerAdapter(): DataListRecyclerViewAdapter<Any, RecyclerView.ViewHolder> {
        adapter.emptyText = resources.getString(R.string.info_no_data)
        adapter.onSourceClick = { data, _ ->
            context?.let { NewsListActivity.launchIntent(it, data.id) }
        }
        return adapter as DataListRecyclerViewAdapter<Any, RecyclerView.ViewHolder>
    }

    override fun initRecyclerLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        // Initial behaviour
        enableSwipeToRefresh()
        disableInfiniteScrolling()

        // View bindings
        txt_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // Do nothing
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // Do nothing
            }

            override fun afterTextChanged(s: Editable) {
                val query = s.toString().trim()
                if (!query.isBlank()) {
                    adapter.setDataListFilter {
                        return@setDataListFilter ((it.name?.contains(query, ignoreCase = true) ?: false)
                                || (it.description?.contains(query, ignoreCase = true) ?: false))
                    }
                } else {
                    adapter.setDataListFilter(null)
                }
            }
        })
    }

    private fun initViewModel() {
        viewModel.resource.observe(viewLifecycleOwner, Observer {
            updateResource(it)
        })
        viewModel.dataList.observe(viewLifecycleOwner, Observer {
            updateDataList(it)
        })
        lifecycle.addObserver(viewModel)
    }

    override fun fetchData() {
        viewModel.fetchData(category)
    }

    override fun onSwipeToRefresh() {
        viewModel.onRefresh()
        fetchData()
    }

    companion object {

        /** Obtain this fragment */
        fun newInstance(category: String): SourceListFragment {
            val fragment = SourceListFragment()
            val args = Bundle()
            args.putString("category", category)
            fragment.arguments = args
            return fragment
        }
    }
}
