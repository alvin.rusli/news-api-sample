package com.alvinrusli.mandiritasknews.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alvinrusli.mandiritasknews.R
import com.alvinrusli.mandiritasknews.activity.SourceListActivity
import com.alvinrusli.mandiritasknews.adapter.recycler.CategoryAdapter
import com.alvinrusli.mandiritasknews.adapter.recycler.core.DataListRecyclerViewAdapter
import com.alvinrusli.mandiritasknews.fragment.core.DataListFragment
import com.alvinrusli.mandiritasknews.viewmodel.CategoryListViewModel

class CategoryListFragment private constructor() : DataListFragment() {

    private val adapter = CategoryAdapter()

    private val viewModel by lazy { ViewModelProviders.of(this).get(CategoryListViewModel::class.java) }

    @Suppress("UNCHECKED_CAST")
    override fun initRecyclerAdapter(): DataListRecyclerViewAdapter<Any, RecyclerView.ViewHolder> {
        adapter.emptyText = resources.getString(R.string.info_no_data)
        adapter.onCategoryClick = { data, _ ->
            context?.let { SourceListActivity.launchIntent(it, data.category ?: "") }
        }
        return adapter as DataListRecyclerViewAdapter<Any, RecyclerView.ViewHolder>
    }

    override fun initRecyclerLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()

        // Initial behaviour
        enableSwipeToRefresh()
        disableInfiniteScrolling()
    }

    private fun initViewModel() {
        viewModel.resource.observe(viewLifecycleOwner, Observer {
            updateResource(it)
        })
        viewModel.dataList.observe(viewLifecycleOwner, Observer {
            updateDataList(it)
        })
        lifecycle.addObserver(viewModel)
    }

    override fun fetchData() {
        viewModel.fetchData()
    }

    override fun onSwipeToRefresh() {
        viewModel.onRefresh()
        fetchData()
    }

    companion object {

        /** Obtain this fragment */
        fun newInstance(): CategoryListFragment {
            return CategoryListFragment()
        }
    }
}
