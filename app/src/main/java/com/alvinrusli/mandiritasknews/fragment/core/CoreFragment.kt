package com.alvinrusli.mandiritasknews.fragment.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment

abstract class CoreFragment : Fragment() {

    abstract val viewRes: Int?

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Initialize the fragment's view binding
        return if (viewRes == null) null
        else inflater.inflate(viewRes!!, container, false)
    }
}
