package com.alvinrusli.mandiritasknews.api

import com.alvinrusli.mandiritasknews.model.news.NewsListResponse
import com.alvinrusli.mandiritasknews.model.source.SourceListResponse
import com.alvinrusli.mandiritasknews.util.Config
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

class APIService {

    val retrofitInterface: APIInterface by lazy {
        val retrofit = Retrofit.Builder()
                .baseUrl(Config.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClientHelper().initOkHttpClient())
                .build()
        retrofit.create(APIInterface::class.java)
    }

    interface APIInterface {

        @GET("v2/sources")
        fun getCategories(
            @Query("apiKey") apiKey: String?,
            @Query("category") category: String?)
                : Call<SourceListResponse>

        @GET("v2/everything")
        fun getNews(
            @Query("apiKey") apiKey: String?,
            @Query("pageSize") pageSize: Int?,
            @Query("page") page: Int?,
            @Query("sources") sources: String?,
            @Query("q") query: String? = null)
                : Call<NewsListResponse>
    }
}
